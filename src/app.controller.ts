import { BadRequestException, Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

import { LoteriaCardsService } from './loteria-cards/loteria-cards.service';

@Controller('loteria')
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly loteriaCardsService: LoteriaCardsService,
  ) {}

  @Get('tables/create')
  async createTables(@Query('cantidad') quantity: number): Promise<string[][]> {
    console.log('quantity', quantity);
    if (!quantity || quantity <= 0) {
      throw new BadRequestException('quantity is required');
    }
    const loteriaCards = await this.loteriaCardsService.getLoteriaCards();
    return this.appService.generateLoteriaTable(loteriaCards, quantity);
  }
}
