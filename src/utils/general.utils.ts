export default class GeneralUtils {
  static shuffleSorting(array: string[]): string[] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  static checkForRepeated(existingTables: string[][], newTable: string[]) {
    let isRepeated = false;
    for (let i = 0; i < existingTables.length; i++) {
      if (existingTables[i].every((element) => newTable.includes(element))) {
        isRepeated = true;
        break;
      }
    }

    return isRepeated;
  }
}
