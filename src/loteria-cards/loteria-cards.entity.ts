import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('loteria_cards')
export default class LoteriaCards {
  @PrimaryColumn()
  id: number;

  @Column()
  name: string;
}
