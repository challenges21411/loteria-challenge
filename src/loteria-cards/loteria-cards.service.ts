import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import LoteriaCards from './loteria-cards.entity';

@Injectable()
export class LoteriaCardsService {
  constructor(
    @InjectRepository(LoteriaCards)
    private loteriaCardsRepository: Repository<LoteriaCards>,
  ) {}

  getLoteriaCards(): Promise<LoteriaCards[]> {
    return this.loteriaCardsRepository.find();
  }
}
