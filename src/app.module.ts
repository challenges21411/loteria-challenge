import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { dataSourceOptions } from './database/db-connection';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoteriaCardsService } from './loteria-cards/loteria-cards.service';
import LoteriaCards from './loteria-cards/loteria-cards.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(dataSourceOptions),
    TypeOrmModule.forFeature([LoteriaCards]),
  ],
  controllers: [AppController],
  providers: [AppService, LoteriaCardsService],
})
export class AppModule {}
