import { Injectable } from '@nestjs/common';

import LoteriaCards from './loteria-cards/loteria-cards.entity';
import GeneralUtils from './utils/general.utils';

@Injectable()
export class AppService {
  generateLoteriaTable(
    loteriaCards: LoteriaCards[],
    quantity: number,
  ): string[][] {
    const tablas: string[][] = [];

    const loteriaCardsNames = loteriaCards.map((card) => card.name);

    let i = 0;
    while (i < quantity) {
      const tabla = GeneralUtils.shuffleSorting(loteriaCardsNames).slice(0, 16);
      const isRepeated = GeneralUtils.checkForRepeated(tablas, tabla);
      if (!isRepeated) {
        tablas.push(tabla);
        i++;
      }
    }
    return tablas;
  }
}
