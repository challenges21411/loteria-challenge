## Description

Code challenge - Loteria tables creation

## Installation

```bash
$ yarn install
```

## Running the app

```bash
$ sudo docker-compose build
$ docker-compose run -d
$ yarn seed:run
```

## Stopping the app
```bash
$ docker-compose down
```